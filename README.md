# progetto_ProSviSo2018

First assignment

Team:
+ Carlo Radice 807159
+ Costantino Carta 808417

Link alla repository su GitLab:
- https://gitlab.com/CarloRadice/progetto_prosviso2018

## Applicazione

L'applicazione è un programma python che permette la validazione di inserimento di libri e prestiti in un database di una biblioteca.

Il database è composto da tre tabelle (Utente, Libro, Prestito).

Il contenuto delle tabelle è il seguente:
+ Utente: contiene i dati degli utenti iscritti al servizio bibliotecario; 
+ Libro: contiene i dati dei libri presenti nella biblioteca;
+ Prestito: contiene tutte le istanze di prestiti da parte degli utenti.

Il database utilizzato è MySQL che si interfaccia con l'applicazione python tramite la libreria "mysql.connector".
L'applicazione python si occupa della validazione dei dati inseriti all'interno del database.

## DevOps

+ Containerizzazione/Virtualizzazione
+ Integrazione e Sviluppo continuo CI/CD

## Containerizzazione/Virtualizzazione

Utilizzo di due container con due docker image:
+ Database : docker image mySQL:5.7
+ Applicazione : docker image Python 2.7

## Integrazione e Sviluppo continuo CI/CD

Sfruttiamo la disponibilità dello strumento CI/CD offerto da GiTLab.

Vengono eseguite due fasi:
+ Build
+ Test

Nella fase di build viene creato il docker dell'applicazione che viene poi pushato
- docker build - tregistry.gitlab.com/carloradice/progetto_prosviso2018
- docker push $CONTAINER_IMAGE

Nella fase di test come servizio viene settata l'immagine "mysql:5.7"
E tramite uno script viene lanciata la classe test
 - python ./application/test.py


La classe test in python viene eseguita ad ogni commit tramite il file di configurazione .gitlab-ci.yml.
Essa permette di validare i dati inseriti all'interno della base di dati