# Use an official Python runtime as a parent image
FROM python:2.7

# Set the working directory to /app
WORKDIR /app

RUN apt-get update
#RUN apt-get install python-mysqldb
RUN pip install mysql-connector-python

# Copy the current directory contents into the container at /app
COPY . /app

# Run app.py when the container launches
CMD ["python", "./application/test.py"]

