import functions
import initializer
import mysql.connector
from mysql.connector import errorcode

def test_check_username():
	usernames = ['pippo','PLUTO','Carlino','marQuez','dGs','4&%3ref','3452','bello gu']
	is_valid = [True,True,True,True,True,False,False,False]
	for username, valid in zip(usernames, is_valid):
		try:
			assert functions.check_utente(username) == valid
		except AssertionError:
			print('Assertion username error')

def test_check_nome():
	nomi = ['mario', 'marco%andrea' ,'marco-andrea','ele352onora','Giovanni','4232','Marco Apost']
	is_valid = [True,False,False,False,True,False,True]
	for nome, valid in zip(nomi, is_valid):
		try:
			assert functions.check_nome(nome) == valid
		except AssertionError:
			print('Assertion name error, wrong string:')
			print(nome)


def test_check_cognome():
	nomi = ['mario', 'marco%andrea' ,'marco-andrea','ele352onora','Giovanni','4232','Marco Apost']
	is_valid = [True,False,False,False,True,False,False]
	for nome, valid in zip(nomi, is_valid):
		try:
			assert functions.check_cognome(nome) == valid
		except AssertionError:
			print('Assertion surname error')
			print(nome)
			
def test_check_isbn():
    isbn = ['01249','3','39495823','-300','mandaurino']
    is_valid = [True,True,True,False,False]
    for isbn,valid in zip(isbn, is_valid):
        try:
            assert functions.check_libro(isbn) == valid
        except AssertionError:
            print('Assertion ISBN error')
            print(isbn)
            

print("Inizio testing")
print("Creazione database..")
initializer.initializeDB()
print('Database creato e riempito con qualche tupla!')
test_check_username()
test_check_nome()
test_check_cognome()
test_check_isbn()
print("Aggiunta di un nuovo prestito...")
cursor,db = functions.retCursor()
sql = "INSERT INTO prestito (isbn,titolo,username,data) VALUES (%s,%s,%s,%s)"
val = [('2','La foresta nera','co','2018-11-05')]
cursor.executemany(sql,val)
db.commit()
cursor.execute("SELECT * FROM prestito")
print("\nTabella prestito:\n")
print(cursor.fetchall())
db.close()
print("Fine testing!!")
