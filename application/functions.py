#!/usr/bin/python

import mysql.connector
from mysql.connector import errorcode
import datetime

def retCursor():
	database = mysql.connector.connect(host='mysql', password='password',  user='root')
	cursor = database.cursor()
	cursor.execute("use biblioteca;")
	return cursor, database
	
def check_utente(username):
	for i in range (0, len(username)):
#   	if((username[i] >= 'a' and username[i] <= 'z') or (username[i] >= 'A' and username[i] <= 'Z')):
#        	return True
#    return False
		if not (username[i] >= 'a' and username[i] <= 'z') and not (username[i] >= 'A' and username[i] <= 'Z'):
			return False
	return True

	
def check_utente_db(username):
	cursor, database = retCursor()
#	cursor.execute("SELECT username FROM utenti WHERE username = '%s')", %(username))
	cursor.execute("SELECT username FROM utenti")
	results = cursor.fetchall()
	b = False
	for row in results:
		if row[0] == username:
			b = True
	return b
	
def check_nome(nome):
	for i in range (0, len(nome)):
		if not (nome[i] >= 'a' and nome[i] <= 'z') and not (nome[i] >= 'A' and nome[i] <= 'Z') and not nome[i] == ' ':
			return False
	return True
	
def check_cognome(cognome):
	for i in range (0, len(cognome)):
		if not (cognome[i] >= 'a' and cognome[i] <= 'z') and not (cognome[i] >= 'A' and cognome[i] <= 'Z'):
			return False
	return True
	
def check_libro(isbn):
    if(isbn.isdigit()): 
        if(isbn >= 0):
            return True
    else:
        return False
	
def check_libro_db(isbn):
	cursor, database = retCursor()
	cursor.execute("SELECT isbn FROM libri")
	results = cursor.fetchall()
	b = False
	for row in results:
		if row[0] == isbn:
			b = True
	return b
	
def add_username(username, nome, cognome):
	cursor, database = retCursor()
	try:
		cursor.execute("INSERT INTO utenti (username, nome, cognome) VALUES (%s, %s, %s);" %(username, nome, cognome))
		database.commit()
	except MySQLdb.Error as e:
		return error
	return True
   
def get_titolo(isbn):
	cursor, database = retCursor()
	cursor.execute("SELECT titolo FROM libri WHERE isbn = '%s'" %(isbn))
	titolo = cursor.fetchone()
	return titolo[0]
  
def add_prestito(isbn, titolo, username):
	cursor, database = retCursor()
	try:
		cursor.execute("INSERT INTO prestito(isbn, titolo, username, data) VALUES ('%d', '%s', '%s', '%Y-%m-%d')" %(isbn, titolo, username, datetime.datetime.now()))
		db.commit()
	except:
   		db.rollback()
