#!/usr/bin/python

import mysql.connector
from mysql.connector import errorcode

def initializeDB():
# 	#open database connection
 	db = mysql.connector.connect(host='mysql', password='password',  user='root')

# 	#prepare a cursor object
 	cursor = db.cursor()

# 	#if database Biblioteca doesn't exist we'll create it
 	cursor.execute("CREATE DATABASE IF NOT EXISTS biblioteca;")
 	cursor.execute("USE biblioteca;")

# 	#if tables doesn't exists, create and open it
 	cursor.execute("CREATE TABLE IF NOT EXISTS utenti(username VARCHAR(255) primary key, nome VARCHAR(255), cognome VARCHAR(255));")
 	cursor.execute("CREATE TABLE IF NOT EXISTS libri(isbn INT primary key, titolo VARCHAR(25), autore VARCHAR(25), anno YEAR(4),genere VARCHAR(20),casaEditrice VARCHAR(25));")
 	cursor.execute("CREATE TABLE IF NOT EXISTS prestito(isbn INT, titolo VARCHAR(25), username VARCHAR(255) primary key,data DATE, FOREIGN KEY (isbn) REFERENCES libri(isbn), FOREIGN KEY (username) REFERENCES utenti(username));")

    ##fill tables with some values
# 	#try
 	sql="INSERT INTO utenti(username, nome, cognome) VALUES(%s, %s, %s)"
 	val= [('carletto96','Carlo','Radice'),('co','Costantino','Carta')]
 	cursor.executemany(sql,val)
 	db.commit()
# 	#except:
# 	#	db.rollback()
# 	#try:
 	sql="INSERT INTO libri(isbn, titolo, autore, anno, genere, casaEditrice) VALUES(%s, %s, %s, %s, %s, %s)"
# #	val= [(23945932134, La foresta nera, Radice Carlo, 2013, Thriller, Unimib),(48589359538, Misery non deve morire, Steven King, 2001, Thriller, Bompiani),(21324354566, I ragazzi della via Pal, Ferenc Molnar, 1906, Mondadori),(21324354566, Piccoli Brividi, Augustus Glop, 1987, Mondadori),(1111, Curb detection on urban environments using LiDAR pointclouds, Costantino Carta, 2018, Mondadori)]
 	val= [('2','La foresta nera','Radice Carlo','2013','Thriller','Unimib')]
 	cursor.executemany(sql,val)
 	db.commit()
# 	#except:
# 	#	db.rollback()
    
 	cursor.execute("SELECT * FROM utenti")
 	print("\nTabella utenti:\n")
 	print(cursor.fetchall())
	cursor.execute("SELECT * FROM libri")
	print("\nTabella libri:\n")
 	print(cursor.fetchall())
 	cursor.execute("SELECT * FROM prestito")
 	print("\nTabella prestito:\n")
 	print(cursor.fetchall())
		
	db.close()

 	print('Creazione database andata a buon fine')